const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    var sources = std.ArrayList([]const u8).init(b.allocator);

    {
        var dir = try std.fs.cwd().openDir("src", .{ .iterate = true });

        var walker = try dir.walk(b.allocator);
        defer walker.deinit();

        const allowed_exts = [_][]const u8{ ".c", ".cpp", ".cxx", ".c++", ".cc" };
        while (try walker.next()) |entry| {
            const ext = std.fs.path.extension(entry.basename);
            const include_file = for (allowed_exts) |e| {
                if (std.mem.eql(u8, ext, e))
                    break true;
            } else false;
            if (include_file) {
                var file = b.pathJoin(&.{"src", entry.path});
                try sources.append(file);
            }
        }
    }
   
    const exe = b.addExecutable("space-shooter", null);
    exe.addIncludeDir("include");
    exe.addCSourceFiles(sources.items, &[_][]const u8{
        "-Wall",
        "-Wextra",
        "-g"
    });
    exe.linkLibC();
    exe.linkSystemLibrary("raylib");
    exe.install();
}
