#pragma once
#include <raylib.h>
#include <cute/cute_c2.h>

struct GameState;

typedef enum 
    { ENTITY_PLAYER
    , ENTITY_ENEMY
    , ENTITY_PARTICLE
    , ENTITY_BULLET
    } 
EntityType;


typedef struct Entity
    { EntityType    type
    ; int           id
    ; c2v           position
    ; Color         color
    ; void        (*destroy) (struct Entity *)
    ; void        (*update)  (struct GameState *, struct Entity *)
    ; void        (*draw)    (const struct Entity *)
    ; }
Entity;

typedef void (*DestroyFn)(Entity *);
typedef void (*UpdateFn)(struct GameState *, Entity *);
typedef void (*DrawFn)(const Entity *);