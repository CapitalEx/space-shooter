// TODO: implement particle.c
#include "particle.h"
#include "cute/cute_c2.h"
#include "entity.h"
#include "../game_state.h"
#include <raylib.h>
#include <stdlib.h>
#include <capital/hacks.h>

Particle* particle_create(ParticleType type, float x, float y, float vx, float vy)
{
    Particle* p = (Particle*) malloc(sizeof(Particle));
    *p = (Particle)
        { .super =  
            { .type = ENTITY_PARTICLE
            , .position = {x, y}
            , .destroy = (DestroyFn) particle_destroy
            , .update = (UpdateFn) particle_update
            , .draw = (DrawFn) particle_draw
            }
        , .type = type
        , .vx = vx
        , .vy = vy
        };
    
    return p;
}

void particle_destroy(Particle* p)
{
    free(p);
}

void particle_draw(const Particle* p)
{
    switch (p->type)
    {
    case PARTICLE_STAR:
    case PARTICLE_PLANET:
    case PARTICLE_BOX:
        {
            int random = GetRandomValue(127, 255);
            Color c = {random, random, random};
            DrawPoly(to_Vector2(p->super.position), 4, 30, 0, GRAY);
            break;
        }
    }
}


bool offscreen(Particle* p)
{
    return p->super.position.x < -100
        || p->super.position.x > GetScreenWidth() + 100
        || p->super.position.y < -100
        || p->super.position.y > GetScreenHeight() + 100;
}

void particle_update(GameState* gs, Particle* p)
{
    particle_move(p, p->vx * GetFrameTime(), p->vy * GetFrameTime());
    if (offscreen(p)) {
        queue_free(gs, as(Entity, p));
    }
}


void particle_move(Particle* p, float dx, float dy)
{
    p->super.position.x += dx;
    p->super.position.y += dy;
}