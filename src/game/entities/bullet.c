#include <raylib.h>
#include <stdlib.h>
#include <math.h>
#include <raylib.h>
#include <capital/hacks.h>
#include "bullet.h"
#include "cute/cute_c2.h"
#include "entity.h"

const c2v min = {-2.5, -2.5};
const c2v max = {2.5, 2.5};

Bullet* bullet_create(float x, float y) {
    c2v pos = c2V(x, y);
    Bullet* bullet = malloc(sizeof(Bullet));
    *bullet = (Bullet) 
        { .super = 
            { .type     = ENTITY_BULLET 
            , .position = pos
            , .destroy  = (DestroyFn) bullet_destroy
            , .update   = (UpdateFn) bullet_update
            , .draw     = (DrawFn) bullet_draw
            }
        , .collision = 
            { .type       = C2_TYPE_AABB
            , .shape.aabb = 
                { .max = c2Add(pos, min)
                , .min = c2Add(pos, max)
                }
            }
        };
    return bullet;
}

void bullet_destroy(Bullet* bullet) {
    free(bullet);
}

void bullet_update(struct GameState* gs, Bullet* bullet){
    float dy = -BULLET_SPEED * GetFrameTime();
    bullet_move(bullet, 0, dy);

    if (bullet->super.position.y < -10) {
        queue_free(gs, as(Entity, bullet));
    }
}

void bullet_draw(const Bullet* bullet) {
    const c2AABB* aabb = &bullet->collision.shape.aabb;

    float width = fabs(aabb->max.x - aabb->min.x);
    float height = fabs(aabb->max.y - aabb->min.y);

    DrawRectangle(aabb->max.x, aabb->max.y, width, height, BLUE);
}

void bullet_move(Bullet* bullet, float dx, float dy) {
    c2v dpos = c2V(dx, dy);
    c2AABB* aabb = &bullet->collision.shape.aabb;

    as(Entity, bullet)->position = c2Add(as(Entity, bullet)->position, dpos);
    aabb->max = c2Add(aabb->max, dpos);
    aabb->min = c2Add(aabb->min, dpos);
}