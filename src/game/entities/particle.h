#pragma once
#include "entity.h"
#include "../game_state.h"
#include <raylib.h>

typedef enum 
    { PARTICLE_STAR
    , PARTICLE_PLANET
    , PARTICLE_BOX
    }
ParticleType;

typedef struct
    { Entity       super
    ; ParticleType type
    ; float        vx
    ; float        vy
    ; }
Particle;


Particle* particle_create(ParticleType type, float x, float y, float vx, float vy);
void      particle_destroy(Particle* p);

void particle_draw(const Particle*);
void particle_update(struct GameState* gs, Particle*);
void particle_move(Particle*, float, float);