#pragma once
#include <cute/cute_c2.h>

typedef union 
    { c2Circle  circle
    ; c2AABB    aabb
    ; c2Capsule capsule
    ; c2Poly    poly 
    ; }
CollisionShape;

typedef struct
    { C2_TYPE        type
    ; CollisionShape shape
    ; }
Collision2D;