#pragma once
#include "entity.h"
#include "collision.h"
#include "../game_state.h"


typedef struct Player
    { Entity      super
    ; Collision2D collision
    ; int health
    ; }
Player;


Player* player_create(float, float);

void player_destroy(Player*);
void player_update(struct GameState*, Player*);
void player_draw(const Player*);
void player_move(Player*, float x, float y);