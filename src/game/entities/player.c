#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <raylib.h>
#include <capital/hacks.h>
#include <cute/cute_c2.h>
#include "entity.h"
#include "bullet.h"
#include "../game_state.h"
#include "player.h"

#define Y 550

Player* player_create(float x, float y)
{
    Player* p = (Player*) malloc(sizeof(Player));
    *p = (Player)
        { .super = 
                { .position = (c2v){x, Y}
                , .color    = GREEN
                , .type     = ENTITY_PLAYER
                , .destroy  = (DestroyFn) player_destroy
                , .update   = (UpdateFn)  player_update
                , .draw     = (DrawFn)    player_draw
                }
        , .collision = {}  
        , .health    = 3
        };
    
    Entity *e = as(Entity, p);
    {
        p->collision.type = C2_TYPE_POLY;

        
        c2Poly *poly = &p->collision.shape.poly;

        poly->count = 3;
        for (int i = 0; i < 3; i++) {
            float a = 2 * PI * (i / 3.0) - PI / 2;
            float x = cosf(a) * 20;
            float y = -sinf(a) * 20;
            poly->verts[i] = c2Sub(e->position, (c2v){x, y});
        }
        c2MakePoly(poly);
    }

    
    return p;
}

void player_destroy(Player* p)
{
    free(p);
}

void player_update(struct GameState* gs, Player* p) {
    Vector2 mpos = GetMousePosition();
    player_move(p, mpos.x, Y);

    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) 
    {
        const c2Poly *poly = &p->collision.shape.poly;
        Bullet* b = bullet_create(poly->verts[0].x, poly->verts[0].y);
        add_entity(gs, as(Entity, b));
    }
}


void player_draw(const Player* p) {
    const c2Poly *poly = &p->collision.shape.poly;
    DrawTriangle(
           to_Vector2(poly->verts[0])
        ,  to_Vector2(poly->verts[1])
        , to_Vector2(poly->verts[2])
        , YELLOW);
}

void player_move(Player* p, float x, float y) {

    Entity* e = as(Entity, p);
    e->position = (c2v){x, y};
    c2Poly *poly = &p->collision.shape.poly;
    for (int i = 0; i < 3; i++) {
        float a = 2 * PI * (i / 3.0) - PI / 2;
        float x =  cosf(a) * 20;
        float y = -sinf(a) * 20;
        poly->verts[i] = c2Sub(e->position, (c2v){x, y});
    }

    c2Norms(poly->verts, poly->norms, poly->count);
}