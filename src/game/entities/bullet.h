#pragma once
#include "entity.h"
#include "collision.h"
#include "../game_state.h"

#define BULLET_SPEED 300.0

typedef struct Bullet
    { Entity      super
    ; Collision2D collision
    ; }
Bullet;

Bullet* bullet_create(float, float);

void bullet_destroy(Bullet*);
void bullet_update(struct GameState*, Bullet*);
void bullet_draw(const Bullet*);
void bullet_move(Bullet*, float, float);