#include <cute/cute_c2.h>
#include "entity.h"

typedef struct
    { Entity super
    ; c2AABB collision
    ; }
Enemy;


Enemy* enemy_create(float x, float y);
void enemy_destroy(Enemy*);

void enemy_draw(const Enemy*);
void enemy_update(Enemy*);
void enemy_move(Enemy*, float x, float y);
