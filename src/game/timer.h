#include <stdbool.h>

typedef void (*TimerCallback)(void);
struct GameState;

typedef struct {
    int           id;
    float         time_left;
    float         duration;
    bool          once;
    TimerCallback callback;
} Timer;

GameState* timer_create(float duration, bool once, TimerCallback cb);
void timer_destory(Timer *);
void timer_update(struct GameState*, Timer *);