#include "game_state.h"
#include "entities/entity.h"
#include "entities/particle.h"
#include "entities/player.h"

#include <capital/hacks.h>
#include <raylib.h>
#include <stb/ds.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

void draw_all(EntityMap* ents)
{
    for(int i = 0; i < hmlen(ents); i++)
    {
        ents[i].value->draw(ents[i].value);
    }
}


void update_all(GameState *gs, EntityMap* ents)
{
    for (int i = 0; i < hmlen(ents); i++)
    {
        ents[i].value->update(gs, ents[i].value);
    }
}

void destroy_all(EntityMap* ents)
{
    for (int i = 0; i < hmlen(ents); i++)
        ents[i].value->destroy(ents[i].value);
} 


GameState *game_state_create()
{
    GameState *gs = (GameState *)malloc(sizeof(GameState));
    (*gs) = (GameState)
        { .mode = MODE_MENU
        , .running = true
        };

    return gs;
}


void game_state_destroy(GameState * gs)
{
    printf("DESTROYING GAME STATE\n");
    if (hmlen(gs->entities) > 0)
    {
        destroy_all(gs->entities);
        hmfree(gs->entities);
    }

    if (hmlen(gs->particles) > 0 )
    {
        destroy_all(gs->particles);
        hmfree(gs->particles);
    }

    if (gs->player) 
    {
        free(gs->player);
        gs->player = NULL;
    }

    free(gs);
}


void state_exit(GameState* gs, GameMode mode)
{
    switch (mode)
    {

    case MODE_MENU:
        menu_exit(gs);
        break;
    case MODE_GAME_OVER:
        game_over_exit(gs);
        break;
    case MODE_PLAY:
        play_exit(gs);
        break;
    }
}


void state_enter(GameState* gs, GameMode mode)
{
    switch (mode)
    {
    case MODE_MENU:
        menu_enter(gs);
        break;
    case MODE_GAME_OVER:
        game_over_enter(gs);
        break;
    case MODE_PLAY:
        printf("Entering Play\n");
        play_enter(gs);
        break;
    }
    gs->mode = mode;
}


void state_change(GameState* gs, GameMode mode)
{
    state_exit(gs, gs->mode);
    state_enter(gs, mode);
}


void state_update(GameState *gs)
{
    switch (gs->mode)
    {
    case MODE_MENU:
        menu_update(gs);
        break;
    case MODE_GAME_OVER:
        game_over_update(gs);
        break;
    case MODE_PLAY:
        play_update(gs);
        break;
    }
}


void state_draw(GameState *gs)
{
    switch (gs->mode)
    {
    case MODE_MENU:
        menu_draw(gs);
        break;
    case MODE_GAME_OVER:
        game_over_draw(gs);
        break;
    case MODE_PLAY:
        play_draw(gs);
        break;
    }
}

bool find_and_delete(EntityMap* map, int key) {
    Entity* entry = hmget(map, key);
    if (entry) {
        entry->destroy(entry); hmdel(map, key); return true;
    }
    return false;
}

void state_clean_up(GameState *gs)
{

    while (arrlen(gs->freeable_ids) > 0) {
        int id = arrpop(gs->freeable_ids);
        if (find_and_delete(gs->entities, id))
            continue;
        
        if (find_and_delete(gs->particles, id))
            continue;
    }
}

void menu_enter(GameState* gs)
{
    // TODO
}


void menu_exit(GameState* gs)
{
    // TODO
}

#define TITLE    "SPACE SHOOTER"
#define PLAY     "PLAY"
#define EXIT     "Exit"
#define FONTSIZE 64

bool play_button_hovered(Vector2 mouse_pos) {
    int play_width  = MeasureText(PLAY, FONTSIZE);
    Rectangle play_button = {200, 200, (float) play_width, FONTSIZE};
    return CheckCollisionPointRec(mouse_pos, play_button);
}

bool exit_button_hovered(Vector2 mouse_pos) {
    int exit_width  = MeasureText(EXIT, FONTSIZE);
    Rectangle exit_button = {200, 300, (float) exit_width, FONTSIZE};
    return CheckCollisionPointRec(mouse_pos, exit_button);;
}

void menu_draw(GameState* gs)
{ 
    draw_all(gs->particles);

    Vector2 mouse_pos = GetMousePosition();
    bool play_hovered = play_button_hovered(mouse_pos);
    bool exit_hovered = exit_button_hovered(mouse_pos);

    DrawText(TITLE, 100,  50, FONTSIZE, RAYWHITE);
    DrawText(PLAY,  200, 200, FONTSIZE, play_hovered ? YELLOW : RAYWHITE);
    DrawText(EXIT,  200, 300, FONTSIZE, exit_hovered ? YELLOW : RAYWHITE);
}


void menu_update(GameState* gs)
{
    gs->timer += GetFrameTime();
    if (gs->timer > 0.1) {
        gs->timer = 0;
        int px = GetScreenWidth() + 50;
        int py = GetRandomValue(100, 500);
        Particle *p = particle_create(PARTICLE_BOX, px, py, -200, 0);

        add_particle(gs, as(Entity, p));
    }

    update_all(gs, gs->particles);

    Vector2 mouse_pos = GetMousePosition();
    bool play_hovered = play_button_hovered(mouse_pos);
    bool exit_hovered = exit_button_hovered(mouse_pos);

    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && play_hovered) {
        state_change(gs, MODE_PLAY);
    } else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && exit_hovered) {
        gs->running = false;
    }
}

void play_enter(GameState* gs)
{
    printf("Entering Play 2\n");
    gs->player = player_create(400, 500);
    printf("Entering Play 3\n");
}

void play_exit(GameState* gs)
{
    // TODO
}

void play_draw(GameState* gs)
{
    draw_all(gs->particles);
    draw_all(gs->entities);

    Entity* e = as(Entity, gs->player);
    e->draw(e);

    char out[64];
    sprintf(out, "Health: %d", gs->player->health);

    DrawText(out, 25,  25, FONTSIZE / 2, RAYWHITE);
}


void play_update(GameState* gs)
{
    Entity* e;
    e = as(Entity, gs->player);
    e->update(gs, e);
    
    update_all(gs, gs->particles);
    update_all(gs, gs->entities);
}


void game_over_enter(GameState* gs)
{
    // TODO
}


void game_over_exit(GameState* gs)
{
    // TODO
}


void game_over_draw(GameState* gs)
{
    // TODO
}


void game_over_update(GameState* gs)
{
    // TODO
}


void add_particle(GameState* gs, Entity* particle)
{
    particle->id = gs->id++;
    hmput(gs->particles, particle->id, particle);
}


void add_entity(GameState* gs, Entity* e)
{
    e->id = gs->id++;
    hmput(gs->entities, e->id, e);
}


void queue_free(GameState *gs, Entity *e)
{
    arrpush(gs->freeable_ids, e->id);
}