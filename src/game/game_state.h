#pragma once
#include <raylib.h>
#include <cute/cute_c2.h>

#include "entities/player.h"
#include "entities/entity.h"
#include "entities/particle.h"

struct Player;

typedef enum  
    { MODE_MENU
    , MODE_GAME_OVER
    , MODE_PLAY
    } 
GameMode;

typedef struct { int key; Entity* value; } EntityMap;

typedef struct GameState
    { GameMode mode
    ; int      id
    ; bool     running
    ; float    timer
    ; Player*  player
    ; EntityMap*  entities;
    ; EntityMap*  particles
    ; int *      freeable_ids;
    ; }
GameState;
GameState *game_state_create();
void game_state_destroy(GameState *);

void state_exit(GameState* gs, GameMode mode);
void state_enter(GameState* gs, GameMode mode);
void state_change(GameState* gs, GameMode mode);
void state_update(GameState* gs);
void state_draw(GameState* gs);
void state_clean_up(GameState* gs);

void menu_enter(GameState* gs);
void menu_exit(GameState* gs);
void menu_draw(GameState* gs);
void menu_update(GameState* gs);

void play_enter(GameState* gs);
void play_exit(GameState* gs);
void play_draw(GameState* gs);
void play_update(GameState* gs);

void game_over_enter(GameState* gs);
void game_over_exit(GameState* gs);
void game_over_draw(GameState* gs);
void game_over_update(GameState* gs);

void add_particle(GameState* gs, Entity* particle);
void add_entity(GameState* gs, Entity* entity);
void queue_free(GameState* gs, Entity* e);

