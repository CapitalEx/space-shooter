#define CUTE_C2_IMPLEMENTATION
#define STB_DS_IMPLEMENTATION


#include "game/game_state.h"

#include <raylib.h>
#include <stb/ds.h>
#include <cute/cute_c2.h>
#include <capital/hacks.h>
#include <stdio.h>
#include <stdlib.h>



void load() 
{
    
}

void draw() 
{
    
    
}

void update()
{

}



int main(void) 
{
    InitWindow(800, 600, "Hello, World!");
    SetExitKey(0);
    GameState* gs = game_state_create();
    while (gs->running)
    {
        state_update(gs);
        BeginDrawing();

        ClearBackground(BLACK);
        
        state_draw(gs);

        EndDrawing();
        
        state_clean_up(gs);

        if (IsKeyDown(KEY_ESCAPE) || WindowShouldClose())
            gs->running = false;
    }
    
    game_state_destroy(gs);
    CloseWindow();
    return 0;
}