# Space Shooter

A port(?) remake(?) implementation(?) of [my old game](https://love2d.org/forums/viewtopic.php?p=111691#p111691) in C.

# Build Instructions

- Install RayLib 4.2
- Install Zig
- Run `zig build` in project root